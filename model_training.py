import cw_function as cw
import pandas as pd
import os
import numpy as np
import pickle
from tqdm import tqdm

def load_content():
    model_folder = os.path.abspath('Data')
    article_content = pd.read_csv(os.path.join(model_folder, 'article_merge_content_title.csv'),
         lineterminator='\n')
    return article_content

def text_preprocessing(text):
    text = cw.replace_number(text)
    return text

def main():
    tqdm.pandas()
    model_folder = os.path.abspath('Data')

    ''' Load data and preprocessing '''
    df = load_content() #load data
    df.dropna(inplace=True) #drop nan
    print('Start Preprocessing...')
    df['content'] = df.content.progress_apply(text_preprocessing) #clean data
    print('Content Preprocess Finish!')
    print('-'*30)

    ''' Tokenize content'''
    stop_words = cw.load_stopwords()
    print('Start Tokenize...')
    df['tokenize'] = df.content.progress_apply(cw.tokenize_sentence, stopwords=stop_words, number=False)
    print('Tokenize Finish!')
    print('-'*30)

    '''Save Token'''
    print('Save Tokenize File')
    with open(os.path.join(model_folder, "title_content_tokenize"), "wb") as file:
        pickle.dump(df, file)
    print('Save File Finish')
    print('-'*30)

    '''Bow Extracting'''
    print('Start Extracting Words Frequency...')
    token_text = df['tokenize'].tolist()
    bow_vectorizer, bow_feature = cw.bow_extractor(token_text)
    with open(os.path.join(model_folder, "article_word_frequency"), "wb") as file:
        pickle.dump([bow_vectorizer, bow_feature], file)
    print('Word Extracting Finish!')
    print('-'*30)

    '''TF-IDF Transform'''
    print('Start TF-IDF Transformation')
    tfidf_trans, tfidf_feature = cw.tfidf_transformer(bow_feature)
    with open(os.path.join(model_folder, 'article_count_tfidf'), 'wb') as file:
        pickle.dump([tfidf_trans, tfidf_feature], file)
    print('TF-IDF Transform Finish!')

if __name__ == '__main__':
    main()