import jieba
import pandas as pd
import numpy as np
import re
import os
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from bs4 import BeautifulSoup

########################################################################
#Data Clean Function
########################################################################

def replace_number(x):
    big_number_map = {'１':'1', '２':'2', '３':'3', '４':'4', '５':'5', '６':'6', '７':'7', '８':'8', '９':'9', '０':'0'}
    for before, after in big_number_map.items():
        x = str(x)
        x = x.replace(before, after)
    return x

def load_stopwords():
    '''
    讀取Stopwords file
    '''
    data_folder = os.path.abspath('Data')
    stopwords = open(os.path.join(data_folder, 'stop_words.txt')).read().splitlines()
    return stopwords

def clean_article_tag(content):
    '''
    清除文章內容的Tag
    回傳以空白分隔的句子
    '''
    article = []
    soup = BeautifulSoup(content, 'lxml')
    for para in soup.find_all('p'):
        article.append(para.get_text())
    article = ' '.join(article)
    return article

########################################################################
#Data Feature Extraction Function
########################################################################

def tfidf_transformer(bow_matrix):
    '''
    function: 
    將詞頻的matrix丟進這邊run出TF-IDF matrix
    
    Parameter:
    詞頻矩陣

    Return:
    transformer, tfidf_matrix
    '''
    transformer = TfidfTransformer(norm=None, smooth_idf=True, use_idf=True)
    tfidf_matrix = transformer.fit_transform(bow_matrix)
    return transformer, tfidf_matrix

def bow_extractor(corpus, ngram_range=(1,1)):
    '''
    function: 
    將斷好詞並以空白分割的句子，以此計算各詞的詞頻
    
    Parameter:
    以空白分割的句子

    Return:
    vectorizer, features
    '''
    vectorizer = CountVectorizer(min_df=1 , ngram_range=ngram_range)
    features = vectorizer.fit_transform(corpus)
    return vectorizer, features

########################################################################
#Data Tokenize Function
########################################################################

def tokenize_sentence(corpus, stopwords=[], number=True):
    '''
    針對句子分詞
    '''
    token_list = []
    if number:
        pattern = re.compile(r'[\u4e00-\u9fa5_a-zA-Z0-9]+')
    else:
        pattern = re.compile(r'[\u4e00-\u9fa5_a-zA-Z]+')
    split_corpus = pattern.findall(corpus)
    for sen_ in split_corpus:
        seg = jieba.cut(sen_)
        for token in seg:
            if token not in stopwords:
                token_list.append(token)
    sentence = (' ').join(token_list) 
    return sentence

